const BASE_URL = "https://635f4b20ca0fe3c21a991e72.mockapi.io";

const dataJson = localStorage.getItem("carProductListJson");

if (dataJson !== null) {
  const initialArr = JSON.parse(dataJson);

  initialArr.forEach((product) => {
    const initialCartProduct = new Cart(
      product.id,
      product.name,
      product.price,
      product.imageSrc,
      product.qty
    );
    cartProductList.push(initialCartProduct);
  });
  renderCart(cartProductList);
  document.getElementById(`cart-item-number`).innerHTML =
    cartProductList.length;
}

function fetchAllProducts() {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/capstone_api`,
    method: "GET",
  })
    .then(function (res) {
      turnOffLoading();
      DSSP = res.data;
      renderProductList(res.data);
    })
    .catch(function (err) {
      turnOffLoading();
      console.log("err: ", err);
    });
}
fetchAllProducts();
