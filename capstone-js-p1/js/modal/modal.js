class Cart {
  constructor(id, name, price, imageSrc, qty = 1) {
    (this.id = id),
      (this.name = name),
      (this.price = price),
      (this.imageSrc = imageSrc),
      (this.qty = qty);
  }
}
