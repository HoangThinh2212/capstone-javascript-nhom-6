// import Cart from "../modal/modal.js";

// Mảng lưu danh sách sản phẩm hiện có
let DSSP = [];
let cartProductList = [];

const addLocalStorage = function () {
  const carProductListJson = JSON.stringify(cartProductList);
  localStorage.setItem("carProductListJson", carProductListJson);
};

function turnOnLoading() {
  document.getElementById("loading").style.display = "flex";
}

function turnOffLoading() {
  document.getElementById("loading").style.display = "none";
}

function renderProductList(products) {
  var contentHTML = "";
  products.forEach(function (item) {
    var contentDiv = `
    <div class="card" id="card">
      <img src="${item.image}" alt="" class="product-img"/>
      <div class="product-content">
        <p class="product-name">${item.name}</p>
        <p class="product-price">${item.price} <span class="underline">đ</span></p>
        <p class="product-description">${item.desc}</p>
        <p class="product-type" id="product-type"> ${item.type}</p>
        <button onclick="addItem(${item.id})" class="btn-add">Add to cart</button>
      </div>
    </div>
      `;
    contentHTML += contentDiv;
  });
  document.getElementById("product-list").innerHTML = contentHTML;
}

function toggleType(id, data) {
  var typeInput = document.getElementById("type").value;

  let newArr = DSSP.filter((item) => {
    if (typeInput == "All") {
      return fetchAllProducts();
    }
    return item.type === typeInput;
  });
  renderProductList(newArr);
}

var count = 1;
function openNav() {
  count += 1;
  if (count % 2) {
    closeNav();
    count = 1;
  } else {
    document.getElementById("sideNavCart").style.width = "700px";
    document.getElementById("product-list").style.marginRight = "250px";
    document.getElementById("toggleArea").style.marginRight = "250px";
  }
}

function closeNav() {
  count += 1;
  document.getElementById("sideNavCart").style.width = "0";
  document.getElementById("product-list").style.marginRight = "0";
  document.getElementById("toggleArea").style.marginRight = "0";
}

changeQty = (id, qty) => {
  let indexCart = cartProductList.findIndex((item) => item.id === id);
  if (cartProductList[indexCart].qty == 1 && qty == -1) {
    removeItem(id);
  } else {
    cartProductList[indexCart].qty += qty;
  }
  renderCart(cartProductList);
};

function createCartItems(index) {
  let contentHTML = ``;
  let sum = 0;
  let { id, name, price, imageSrc, qty } = index;
  contentHTML += `
  <div class='cart-item'>
    <div class='cart-img'>     
        <img src='${imageSrc}' alt='' />    
    </div>   
    <strong class='name'>${name}</strong>
    <span class='qty-change'>
      <button class="btn-light" onclick="changeQty(${id},${-1})">
        <i class="fa fa-angle-left"></i>
      </button>
      <strong>${qty}</strong>
      <button class="btn-light" onclick="changeQty(${id},${1})">
        <i class="fa fa-angle-right"></i>
      </button>
    </span>    
    <p class='price font-weight-bold'>$ ${price * qty}</p>    
    <button onclick='removeItem(${id})' class='btn-trash'>
      <i class="fa fa-trash"></i>
    </button>  
  </div>`;

  sum = qty * price;
  return { contentHTML, sum };
}

function addItem(id) {
  let name = document.getElementsByClassName(`product-name`)[id - 1].innerText;
  let price = parseFloat(
    document
      .getElementsByClassName(`product-price`)
      [id - 1].innerText.replace("$ ", "")
  );
  let imageSrc = document.getElementsByClassName(`product-img`)[id - 1].src;
  let indexCart = cartProductList.findIndex((cart) => cart.id === id);
  if (indexCart == -1) {
    let cartItem = new Cart(id, name, price, imageSrc, 1);
    cartProductList.push(cartItem);
  } else {
    cartProductList[indexCart].qty += 1;
  }
  addLocalStorage();
  renderCart(cartProductList);
  document.getElementById(`cart-item-number`).innerHTML =
    cartProductList.length;
  return cartProductList;
}

function DisplayCartItems(cartProductList) {
  let strCart = "";
  let totalPrice = 0;
  cartProductList.map((e) => {
    let { sum, contentHTML } = createCartItems(e);
    totalPrice += sum;
    strCart += contentHTML;
  });
  document.getElementById(`cartTotal`).innerText = `Tổng tiền: ${totalPrice} đ`;
  document.getElementById(`cartTotal`).style.color = `#fff`;
  return strCart;
}

function renderCart(cartProductList) {
  document.getElementsByClassName("cartList")[0].innerHTML =
    DisplayCartItems(cartProductList);
}

const btnCheckout = function () {
  cartProductList = [];
  renderCart(cartProductList);
  addLocalStorage();
  document.getElementById(`cart-item-number`).innerHTML =
    cartProductList.length;
};

const removeItem = function (id) {
  let itemIndex = cartProductList.findIndex((item) => item.id === id);
  cartProductList.splice(itemIndex, 1);
  renderCart(cartProductList);
  addLocalStorage();
  document.getElementById(`cart-item-number`).innerHTML =
    cartProductList.length;
};
