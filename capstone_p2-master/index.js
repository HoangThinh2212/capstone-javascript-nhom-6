const BASE_URL = "https://635f4b20ca0fe3c21a991e72.mockapi.io";

const getAllData = function () {
  return axios({
    url: `${BASE_URL}/capstone_api`,
    method: "GET",
  });
};

loaderOn();

const fetchAllData = function () {
  getAllData()
    .then((res) => {
      renderProducts(res.data);
      loaderOff();
    })
    .catch((err) => {
      console.error(err);
      loaderOff();
    });
};
fetchAllData();

const btnAdd = function () {
  const newPhone = takeFormInput();

  if (checkIsValid(newPhone)) {
    newPhone.type = newPhone.name.split(" ")[0];

    loaderOn();
    axios({
      url: `${BASE_URL}/capstone_api`,
      method: "POST",
      data: newPhone,
    })
      .then((res) => {
        resetForm();
        resetModal("btnAdd", "modal");
        fetchAllData();
      })
      .catch((err) => {
        console.error(err);
        loaderOff();
      });
  } else {
    resetModal("btnAdd", "");
    loaderOff();
    return;
  }
  resetModal("btnAdd", "modal");
};

const btnDelete = function (id) {
  loaderOn();
  axios({
    url: `${BASE_URL}/capstone_api/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      fetchAllData();
    })
    .catch((err) => {
      console.error(err);
      loaderOff();
    });
};

const btnModify = function (id) {
  loaderOn();
  showBtnUpdate();
  axios({
    url: `${BASE_URL}/capstone_api/${id}`,
    method: "GET",
  })
    .then((res) => {
      showFormInput(res.data);

      idTobeModified = res.data.id;
      loaderOff();
    })
    .catch((err) => {
      console.error(err);
      loaderOff();
    });
};

let idTobeModified = null;

const btnUpdate = function () {
  loaderOn();
  const tobeUpdatedPhone = takeFormInput();

  if (checkIsValid(tobeUpdatedPhone)) {
    tobeUpdatedPhone.type = tobeUpdatedPhone.name.split(" ")[0];
    axios({
      url: `${BASE_URL}/capstone_api/${idTobeModified}`,
      method: "PUT",
      data: tobeUpdatedPhone,
    })
      .then((res) => {
        resetForm();
        resetModal("btnUpdate", "modal");
        fetchAllData();
      })
      .catch((err) => {
        loaderOff();
        console.error(err);
      });
  } else {
    resetModal("btnUpdate", "");
    loaderOff();
    return;
  }
  resetModal("btnUpdate", "modal");
};

const btnClose = function () {
  resetForm();
};

const btnX = function () {
  resetForm();
};

const btnSearch = function () {
  loaderOn();
  const searchVal = searchEl.value.toLowerCase();

  axios({
    url: `${BASE_URL}/capstone_api`,
    method: "GET",
  })
    .then((res) => {
      const newArr = res.data.filter((phone) => {
        return phone.name.toLowerCase().includes(searchVal);
      });
      renderProducts(newArr);
      loaderOff();
    })
    .catch((err) => {
      console.error(err);
    });
};
