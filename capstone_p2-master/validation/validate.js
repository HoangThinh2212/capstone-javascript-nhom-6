const checkEmpty = function (userInput, idErr) {
  if (!userInput) {
    showMessage(idErr, "Không được để trống");
    return false;
  } else {
    hideMessage(idErr);
    return true;
  }
};

const checkNumber = function (userInput, idErr) {
  const reg = /^\d+$/;
  const isNumber = reg.test(userInput);

  if (isNumber) {
    hideMessage(idErr);
    return true;
  } else {
    showMessage(idErr, "Vui lòng nhập số");
    return false;
  }
};

const checkSpecialChar = function (userInput, idErr, message) {
  const reg = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
  const isSpecialChar = reg.test(userInput);

  if (isSpecialChar) {
    showMessage(idErr, message);
    return false;
  } else {
    hideMessage(idErr);
    return true;
  }
};

const checkImage = function (userInput, idErr) {
  const reg =
    /^(?:(?<scheme>[^:\/?#]+):)?(?:\/\/(?<authority>[^\/?#]*))?(?<path>[^?#]*\/)?(?<file>[^?#]*\.(?<extension>[Jj][Pp][Ee]?[Gg]|[Pp][Nn][Gg]|[Gg][Ii][Ff]))(?:\?(?<query>[^#]*))?(?:#(?<fragment>.*))?$/gm;

  const isImage = reg.test(userInput);

  if (!isImage) {
    showMessage(idErr, "Url không hợp lệ");
    return false;
  } else {
    hideMessage(idErr);
    return true;
  }
};

const checkIsValid = function (product) {
  console.log(product);
  const name =
    checkEmpty(product.name, "nameErr") &&
    checkSpecialChar(product.name, "nameErr", "Tên không hợp lệ");

  const price =
    checkEmpty(product.price, "priceErr") &&
    checkSpecialChar(product.price, "priceErr", "Giá không hợp lệ") &&
    checkNumber(product.price, "priceErr");

  const img =
    checkEmpty(product.image, "imgErr") && checkImage(product.image, "imgErr");

  const desc = checkEmpty(product.desc, "descErr");

  const isValid = name && price && img && desc;

  return isValid;
};
